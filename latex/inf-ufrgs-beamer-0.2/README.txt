Texto do e-mail que foi enviado:
Ola a todos, 

Consegui resolver alguns probleminhas da vers�o anterior do tema, o que
me motivou a enviar uma nova vers�o. Abaixo o CHANGELOG, e em anexo o
pacote e uma apresenta��o-exemplo. 

Atenciosamente,

Rodrigo Machado



=== CHANGELOG: (vers�o 0.1 para vers�o 0.2) ===

- sem arquivos e imagem externos: todos os gr�ficos s�o feitos em
tikz/pgf :)

- problema do logo no PDFLATEX resolvido.

- divis�o do tema em "inf-ufrgs" e "inf-ufrgs-xelatex" (h� diferen�as
grandes no sistema de coordenadas que compromete a coloca��o do logo e
outros pequenos detalhes).

- espa�amento entre items e linhas levemente ampliado (atrav�s do pacote
setspace).

- se forem colocadas se��es e subse��es no documento, um slide com o
conte�do e o t�pico atual � automaticamente inserido entre as se��es. Se
n�o forem definidas se��es, nada � inserido. 

 Obs: o t�tulo da p�gina inserida � o comando \contentstitle, que pode 
 (e deve) ser redefinido. O default � "Contents". Exemplo:

 \renewcommand{\contentstitle}{Outline of this talk}  


- o slide de conte�do (baseado em se��es/subse��es) � automaticamente
 inserido por 

 \tocINF


- abreviaturas para ambientes de enumera��o foram inclu�das no estilo:

  \be \ee  = \begin{enumeration} e \end{enumeration}
  \bi \ei  =  ... itemize ...
  \bd \ed  =  ... description ...
  \tm      = \item 

Exemplo:

\bi
\tm primeiro item
\tm segundo item 
\tm terceiro item
\ei


---------------------------------------------------------------------------

%%%% INF-UFRGS Theme for Beamer, version 0.2 %%%%%
%                                                %
% Author: Rodrigo Machado  < rma@inf.ufrgs.br >  %
% Date: 17 / Nov / 2010                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


=== O que é ====

Este pacote é um tema para apresentações Beamer. A ideia é ser
esteticamente bonito e seguir o padrão de identidade visual do Instituto de Informática da UFRGS (detalhes em http://www.inf.ufrgs.br).

A proposta do tema é ser limpo e funcional: não há a tradicional barra de navegação do beamer, tampouco barra lateral. Se alguém tiver interesse em me ajudar a melhorá-lo, por favor entre em contato. 



=== Como instalar ===

(USO LOCAL)

Se não quiser instalar o pacote globalmente, basta colocar os arquivos .sty no mesmo diretório da apresentação.

(LINUX)

Descompacte o conteúdo do arquivo "inf-ufrgs-beamer-XXX" em alguma pasta, 
por exemplo

  /home/rodrigo/tex/inf-ufrgs-beamer-XXX

se esta pasta não consta da variável de ambiente $TEXINPUTS, você deve
colocar a seguinte linha no seu arquivo .bashrc ou .profile

  TEXINPUTS=$TEXINPUTS:/home/rodrigo/tex/inf-ufrgs-beamer-XXX


(WINDOWS)

Mesma ideia que no linux, com a diferença que eu particularmente não sei onde ficam os diretórios (nunca usei o MikTeX). Colaborações quanto a isso são bem-vindas.

(MAC)

Idem acima.


=== Como usar ===

No preâmbulo do documento BEAMER, chamar

\usetheme{inf-ufrgs} (para PDFLATEX)

ou então

\usetheme{inf-ufrgs-xelatex} (para XELATEX)

Note que há particularidades: para montar a página inicial, usa-se o comando

\titlepageINF

fora de qualquer frame. Recomenda-se ver a apresentação "exemplo-inf-ufrgs-beamer".



=== Problemas conhecidos (bugs) ===

- A barra vermelha logo abaixo do logo pode ficar levemente deslocada 
em tamanhos diferentes de 10pt.

- Página de titulo podia ser melhor (não sei exatamente como).

- Só há macro para o PPGC (mas é basicamente trivial criar outras macros
  por copy/paste/change).


=== Sugestões/problemas/ideias/soluções? ===

Só mandar um email para rma@inf.ufrgs.br



=== Sobre fontes ===

Há uma série de fontes interessantes para LaTeX. Recomendo ver o LaTeX 
Font Catalogue:

http://www.tug.dk/FontCatalogue/

minhas preferências pessoais são 

1. Baskervald (serifada) (PDFLATEX,XELATEX)

\usefonttheme{serif}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{baskervald}

1. New Century Schoolbook (serifada) (PDFLATEX,XELATEX)

\usefonttheme{serif}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{fouriernc}

2. Myriad Pro e Minion Pro (XELATEX)

\usepackage[no-math]{fontspec}
\usepackage{MnSymbol}
\setsansfont{Myriad Pro}
\setromanfont{Minion Pro}

obs: estas requerem que os arquivos .otf estejam instaladas no sistema. Elas vem junto com o Adobe Reader, e no linux (Ubuntu) basta copiar elas da pasta
   /opt/Adobe/Reader9/Resource/Font
para a pasta 
   ~/.fonts ...



=== CHANGELOG: (versão 0.1 para versão 0.2) ===

- sem arquivos e imagem externos: todos os gráficos são feitos em tikz/pgf :)

- problema com PDFLATEX resolvido.

- divisão em "inf-ufrgs" e "inf-ufrgs-xelatex" (há diferenças grandes no sistema de coordenadas que compromete a colocação do logo e outros pequenos
detalhes).

- espaçamento entre items e linhas levemente ampliado (através do pacote setspace).

- se forem colocadas seções e subseções no documento, um slide com o conteúdo
  e o tópico atual é automaticamente inserido entre as seções. Se não forem
  definidas seções, nada é inserido. 

  Obs: o título da página inserida é o comando \contentstitle, que pode 
  (e deve) ser redefinido. O default é "Contents". Exemplo:

  \renewcommand{\contentstitle}{Outline of this talk}  

- o slide de conteúdo (baseado em seções/subseções) é automaticamente
  chamado por 

  \tocINF


- abreviaturas para ambientes de enumeração foram incluídas no estilo:

   \be \ee  = \begin{enumeration} e \end{enumeration}
   \bi \ei  =  ... itemize ...
   \bd \ed  =  ... description ...
   \tm      = \item 

Exemplo:

\bi
\tm primeiro item
\tm segundo item 
\tm terceiro item
\ei



