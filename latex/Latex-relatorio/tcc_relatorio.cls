
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Classe de estilo latex criada para confec��o de relat�rios de TCC do curso de Ci�ncias da Computa��o da Universidade Regional de Blumenau
% Autor: Dalton Reis (dalton@furb.br)
% Data: 25/05/2009
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\NeedsTeXFormat{LaTeX2e} 
\ProvidesClass{tcc_relatorio}[2009/05/25]

\RequirePackage[english,brazil]{babel}   
\RequirePackage[latin1]{inputenc} 



\usepackage{acronym}  
\RequirePackage{tabularx}  

\LoadClass{article}
\RequirePackage[a4paper,top=2cm,left=2cm,right=2cm,bottom=2cm]{geometry}


%  Comandos para passagem dos par�metros 
\newcommand{\turnoAluno}{}
\newcommand{\numeroAluno}{}
\newcommand{\numeroRelatorio}{}
\newcommand{\numeroQuinzena}{}
\newcommand{\mes}{}
\newcommand{\totalHoras}{}
\newcommand{\nomeOrientando}{}
\newcommand{\tituloTrabalho}{}
\newcommand{\diaSemana}{}
\newcommand{\codigoHorario}{}
\newcommand{\avaliacaoOrientando}{}
\newcommand{\localDataAvaliacaoOrientando}{}
\newcommand{\avaliacaoOrientador}{}
\newcommand{\nomeOrientador}{}
\newcommand{\localDataAvaliacaoOrientador}{}

%  Sele��o da fonte
\renewcommand{\familydefault}{cmss}

% Comando que monta o relat�rio 
\newcommand{\fazerRelatorio}{% 

\noindent
\begin{flushright}
\begin{tabular}[c]{|c|c|c|c|}
\hline
	TURNO: & \turnoAluno & N$^\circ$ & \numeroAluno \\
\hline 
\end{tabular}
\end{flushright}
UNIVERSIDADE REGIONAL DE BLUMENAU \\*
CENTRO DE CI�NCIAS EXATAS E NATURAIS \\*
DEPARTAMENTO DE SISTEMAS E COMPUTA��O \\*
CURSO DE CI�NCIAS DA COMPUTA��O - BACHARELADO \\*
COORD. DO TRABALHO DE CONCLUS�O DE CURSO (TCC) \\*
\vspace{0.5cm}

\begin{large}
RELAT�RIO QUINZENAL N$^\circ$ \hspace{0.1cm} \numeroRelatorio \hspace{0.2cm} DO TRABALHO DE CONCLUS�O DE CURSO(TCC) \\*
\end{large}

\begin{flushright}
\begin{tabular}{|c|c|c|}
\hline
	\numeroQuinzena $^a$ & quinzena do m�s de & \mes \\
\hline 
\end{tabular}
\end{flushright}


\noindent
\begin{tabularx}{\linewidth}{Xr}
	TOTAL DE HORAS TRABALHADAS (ACUMULADAS) AT� A DATA DO PRESENTE RELAT�RIO:  & \fbox{\totalHoras} \\
\end{tabularx} 
\vspace{0.2cm}

\noindent
\textbf{1 IDENTIFICA��O} \\*
\noindent
\begin{tabularx}{\linewidth}{|l|X|}
\hline
	NOME DO ACAD�MICO:  & \nomeOrientando \\
\hline
	NOME DO ORIENTADOR:  & \nomeOrientador \\
\hline
	T�TULO DO TRABALHO: & \tituloTrabalho \\
\hline
	Atendimento do orientador & dia da semana:\diaSemana - c�digo do hor�rio (de 1 a 15):\codigoHorario \\
\hline 
\end{tabularx}
\vspace{0.5cm}

\noindent
\textbf{2	AVALIA��O DO TRABALHO PELO ACAD�MICO (ORIENTANDO)} (Em que ponto est� o trabalho. Se existir dificuldades, especifique-as. Use folha anexa se for necess�rio)


\noindent
\begin{tabularx}{\linewidth}{|X|}
\hline
\parbox[t][2.5cm][t]{\linewidth}{\avaliacaoOrientando} \\
\hline
\end{tabularx}

\noindent
\begin{tabularx}{\linewidth}{|l|X|l|X|}
\hline
 Assinatura do acad�mico: & & Local/Data: & \localDataAvaliacaoOrientando \\
\hline
\end{tabularx}
\vspace{0.5cm}

\noindent
\textbf{3 AVALIA��O DO ANDAMENTO DO TRABALHO PELO ORIENTADOR}

\noindent
\begin{tabularx}{\linewidth}{|X|}
\hline
\parbox[t][2.5cm][t]{\linewidth}{\avaliacaoOrientador} \\
\hline
\end{tabularx}

\noindent
\begin{tabularx}{\linewidth}{|l|X|l|X|}
\hline
 Assinatura do orientador: & & Local/Data: & \localDataAvaliacaoOrientador \\
\hline
\end{tabularx}
\vspace{0.5cm}

\noindent
\textbf{4 RESERVADO A COORDENA��O DO TRABALHO DE CONCLUS�O DE CURSO}

\noindent
\begin{tabularx}{\linewidth}{|X|}
\hline
\parbox[t][2.5cm][t]{\linewidth}{PARECER:} \\
\hline
\end{tabularx}

\noindent
\begin{tabularx}{\linewidth}{| c | c | c | X|l |l Xr ||}
\hline
\multicolumn{3}{|c|}{SITUA��O} \\ %\multicolumn{2}{c}{ eee}\\ 
\hline
APROVADO & REPROVADO & PENDENTE(*) & Assinatura & Local/Data \\
\hline
 & & & & \\
\hline
 & & & & \\
\hline
\end{tabularx}
\vspace{0.1cm}
{\small (*) Quando pendente, o acad�mico necessita falar pessoalmente com o coordenador para regulariza��o da situa��o.} \\
\noindent
\textbf{OBS}.: Primeiramente a avalia��o dever� ser feita pelo Orientando e ap�s pelo Orientador.\\
\begin{flushright}
 {\tiny Relat�rio Quinzenal do TCC  - vers�o 2.2/2008.2 - 25/5/2009 - arquivo: RelQuinzenal-V2-2-2008-2.doc}
\end{flushright}

}

