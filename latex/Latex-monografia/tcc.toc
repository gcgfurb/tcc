\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\c c\~ao}{11}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{11}{section.1.1}
\contentsline {section}{\numberline {1.2}Estrutura do trabalho}{11}{section.1.2}
\contentsline {section}{\numberline {1.3}Observa\c c\~oes gerais}{11}{section.1.3}
\contentsline {section}{\numberline {1.4}Formata\c c\~ao do trabalho}{12}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Disposi\c c\~ao dos elementos}{13}{subsection.1.4.1}
\contentsline {chapter}{\numberline {2}Fundamenta\c c\~ao te\'orica}{14}{chapter.2}
\contentsline {chapter}{\numberline {3}Desenvolvimento}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Requisitos principais do problema a ser trabalhado}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}Especifica\c c\~ao}{15}{section.3.2}
\contentsline {section}{\numberline {3.3}Implementa\c c\~ao}{15}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}T\'ecnicas e ferramentas utilizadas}{15}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Operacionalidade da implementa\c c\~ao}{15}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Resultados e discurss\~ao}{16}{section.3.4}
\contentsline {chapter}{\numberline {4}Conclus\~ao}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Extens\~oes}{17}{section.4.1}
